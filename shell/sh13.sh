#!/bin/sh

# 根据输入的身份证获取出生年月日，以及计算离生日还有多久

# 提示
read -p "please input your cardnum: " cardnum

# 判断身份证格式

res=$(echo $cardnum | grep '^[0-9]\{17\}[0-9x]$')
if [ "$res" == "" ] ; then
	echo "Wrong format!"
	exit 1
fi

birthday=${cardnum:6:8}
echo "your birthday is : $birthday"

# 获取具体的日期 月份天
birthdate=${birthday:4}
birthday_real=$(date +%Y)"$birthdate"

# 计算距离今年生日的时间
declare -i date_birthday_real=$(date --date "$birthday_real" +%s)
declare -i date_now=$(date +%s)
declare -i date_diff=$(($date_birthday_real-$date_now))

if [ "$date_diff" -lt "0" ] ; then
	echo "you had been celebrate birthday"
else
	declare -i date_day=$(( $date_diff/60/60/24 ))
        declare -i date_hours=$(( $(( $date_diff - $date_day*60*60*24 )) /60/60 ))
	echo "you will celebrate birthday after $date_day day and $date_hours hours"
fi  

