#!/bin/sh
# 功能：
#	1. 判断第一个参数是否为hello,如果是则显示"hello ,how are you?"
#	2. 如果没有任何参数，就提示用户必须使用参数
#	3. 如果输入的参数不是hello，就提醒用户仅能使用hello 参数


# program

[ "$#" == 0 ]  && echo "you must input a parameter" && exit 0
if [ "$1" == "hello" ] ; then
	echo "Hello,how are you ?"
else 
	echo "you must use 'hello' parameter"
fi


