#!/bin/sh

iptables -F
iptables -X
iptables -Z

iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 139 -j ACCEPT
iptables -A INPUT -p tcp --dport 21 -j ACCEPT

iptables -P INPUT DROP

/etc/init.d/iptables save
chkconfig --level 35 iptables on
/etc/init.d/iptables restart
