#!/bin/bash
#program:
#	1. 输入一个文件名
#	2. 判断这个文件是否存在，如果不存在则输出"Filename does not exist",并中断程序
#	3. 若文件存在，判断它是目录还是文件，结果输出"Filename is regular file" or "Filename is directory"
#	4. 判断一下，执行者的身份对这个文件或目录所拥有的权限，并输出权限数据

read -p "please enter a filename: " filename
test -z $filename && echo "You must input a filename." && exit 0

test ! -e $filename && echo "Filename '$filename' does not exist" && exit 0

test -f $filename && filetype="regular file"

test -d $filename && filetype="directory"

test -r $filename && perm="readable"
test -w $filename && perm="$perm writable"
test -x $filename && perm="$perm executable"

echo "The filename: $filename is a $filetype"
echo "And the permissons are : $perm"


