#!/bin/sh

read -p "Please input a number for calculate 1!+2!+...+your_input! :" nu

s=0
for (( i=1; i<=$nu; i++ ))
do
	t=1
	for (( j=2; j<=$i; j++ ))
	do
		t=$(( $t*$j ))
	done
	echo "$i ==> $t"
	s=$(( $s+$t))
done

echo "The result is ==> " $s		 
