#!/bin/sh

read -p "Please input a number for calculate 1!+2!+...+your_input! :" nu

s=1
t=1
for (( i=2; i<=$nu; i++ ))
do
	k=0
	for (( j=1; j<=$i; j++ ))
	do
		k=$(( $k+$t ))
	done
	echo "$i ==> $k"
	t=$k
	s=$(( $s+$k))
done

echo "The result is ==> " $s		 
