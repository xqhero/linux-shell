#!/bin/bash

# 历史记录显示执行用户,ip,命令,时间
# 不记录连续的重复条目
# 实时记录
# who am i 执行的结果为 zhouguoqiang pts/0        2021-01-16 15:50 (221.218.212.169)

export PROMPT_COMMAND="history -a"
export HISTCONTROL=ignoredups  #只能剔除连续的重复条目。
USER_IP=$(who am i|awk '{print $NF}'|sed -e 's/[()]//g')
export HISTTIMEFORMAT="${USER} %Y-%m-%d %H:%M:%S ${USER_IP} "

