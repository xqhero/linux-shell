#include <stdio.h>

struct test1 {
    int a;
    long b;
};
struct test4 {
    char a;
    struct test1 b;
    int c;
};

int main(){
    printf("size = %d\n", sizeof(struct test4));
}