#include <stdio.h>
#pragma pack(2)
typedef struct  {
    char b;
    int a;
    short c;
} A;
#pragma pack()

int main(){
    printf("size of type A = %d\n", sizeof(A));
    return 0;
}