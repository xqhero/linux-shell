#include <stdio.h>

typedef struct {
        char a;
        short b;
        int c;
        long d;
        char  e[3];
} test_t;

int main(){
    test_t t;
    char *p = t.e;
    printf("Size = %d\n  a-%p, b-%p, c-%p, d-%p\n  e[0]-%p, e[1]-%p, e[2]-%p\n",
            sizeof(test_t), &t.a, &t.b,
            &t.c, &t.d, p,
            p+1,p+2);
     return 0;
}