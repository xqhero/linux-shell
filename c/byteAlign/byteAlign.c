# include <stdio.h>

typedef struct {
    char a;
    long b;
    char c;
} A;

void main() {
    A t;
    printf("size = %d\n", sizeof(A));
    printf("b-->%p, a-->%p, c-->%p\n",
        &t.b ,
        &t.a ,
        &t.c
    );
}