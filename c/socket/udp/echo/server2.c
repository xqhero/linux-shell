#include <stdio.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>

int main() {
    int sockfd;
    struct sockaddr_in serv1,serv2;

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket");
        return 1;
    }
    memset(&serv1,0,sizeof(serv1));
    serv1.sin_family = AF_INET;
    serv1.sin_port = htons(10001);
    serv1.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(sockfd, (struct sockaddr *)&serv1, sizeof(serv1)) < 0) {
        perror("bind first");
        return 1;
    }

    memset(&serv2,0,sizeof(serv2));
    serv2.sin_family = AF_INET;
    serv2.sin_port = htons(10002);
    serv2.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(sockfd, (struct sockaddr *)&serv2, sizeof(serv2)) < 0) {
        perror("bind second");
        return 1;
    }

    printf("bind success\n");

    return 0;
}