#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>

#define MAXLINE 1024
/*添加消息返回的来源判断*/

int main(int argc, char *argv[]) {
    int sockfd, sport;
    struct sockaddr_in serv;
    char *serv_ip = 0;

    if(argc < 3) {
        printf("please checkout format\n");
        return -1;
    }
    serv_ip = argv[1];
    sport = atoi(argv[2]);

    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket");
        return -1;
    }

    memset(&serv, 0, sizeof(struct sockaddr_in));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(sport);
    inet_pton(AF_INET, serv_ip, &serv.sin_addr);

    int n;
    char sendline[MAXLINE], recvline[MAXLINE];
    struct sockaddr_in saddr;
    char caddr[16];
    socklen_t len = sizeof(struct sockaddr_in);

    while(fgets(sendline, MAXLINE, stdin) != NULL) {
        n = sendto(sockfd, sendline, strlen(sendline), 0, (struct sockaddr *)&serv, sizeof(serv));
        if (n < 0) {
            perror("sendto");
            break;
        }
        
        n = recvfrom(sockfd, recvline, MAXLINE, 0, (struct sockaddr *)&saddr, &len);
        if ( n < 0 ) {
            perror("recvfrom");
            break;
        }
        if(memcmp(&saddr, &serv, sizeof(struct sockaddr_in)) != 0) {
            printf("reply from %s:%d (ignored) \n", inet_ntop(AF_INET, &saddr.sin_addr.s_addr, caddr, 16), ntohs(saddr.sin_port));
            continue;
        }
        recvline[n] = '\0';
        fputs(recvline, stdout);
    }
    return 0;
}