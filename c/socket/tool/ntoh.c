#include <stdio.h>
#include <netinet/in.h>

int main() {
    uint16_t port = 0x1234;
    uint32_t ip = 0x12345678;
    printf("ntohs(%x) = %x\n", port , ntohs(port));
    printf("ntohl(%x) = %x\n", ip , ntohl(ip));
    return 0;
}