#include <stdio.h>
#include <netinet/in.h>
#include <stdint.h>

int main(){

    uint16_t port = 0x1234;
    uint32_t ip = 0x12345678;
    uint16_t net_port = htons(port);
    uint32_t net_ip = htonl(ip);
    printf("htons(%x) = %x\n", port, net_port);
    printf("htonl(%x) = %x\n", ip, net_ip);
    return 0;
}

