#include <stdio.h>

int byteOrder(){
    union {
        short value;
        char bytes[2];
    } u;
    u.value = 0x0102;
    if (u.bytes[0] == 1 && u.bytes[1] == 2) {
        return 1;
    } else if (u.bytes[0] == 2 && u.bytes[1] == 1) {
        return 2;
    } else {
        return -1;
    }
}

int main(){
    int res = byteOrder();
    switch(res){
        case 1:
            printf("Big Endian");
            break;
        case 2:
            printf("Litter Endian");
            break;
        default:
            printf("unknow");
    }
    return 0;
}

