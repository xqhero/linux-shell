#include <stdio.h>
#include <string.h>
#define LEN 2
#define EOL printf("\n");
int main() {
    char c[LEN];
    int i;
    printf("未执行bzero之前:\n");
    for(i=0; i<LEN; i++) {
        printf("%2x ",(unsigned char)c[i]);
    }
    bzero(c,sizeof(c));
    EOL
    printf("执行bzero之后:\n");
    for(i=0; i<LEN; i++) {
        printf("%2x ",c[i]);
    }
    EOL
    return 0;
}