#include <stdint.h>
#include <sys/time.h>

const char *short_opts="i:s:t:w:c:S:";
uint8_t icmp_ttl = 64;
uint16_t icmp_interval = 1;
uint16_t icmp_package_size = 56;
uint16_t icmp_count = 30;
struct timeval icmp_wait_timeout = {10, 0};
int icmp_sndbuf_size = 32 * 2048;
