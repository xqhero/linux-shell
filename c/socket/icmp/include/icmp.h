
#ifndef __ICMP_H
#define __ICMP_H

#include <stdio.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <sys/time.h>
#include <math.h>

#define ICMP_VARS_H

#include <vars.h>
#include <tool.h>


#define ICMP_VERSION    "1.0.0"
#define ICMP_MAX_PACKET_SIZE 65507
#define IP_MAX_TTL    255

#define  ICMP_OK          0
#define  ICMP_ERROR      -1

#define  SENDBUF    2048
#define  RCVBUF     2048

#define  ICMP_BEGIN(h,a,s,ss)   printf("PING %s (%s) %d(%d) bytes of data.\n", (h), (a), (s), (ss))
#define  ICMP_RCVOUTPUT(b,a,seq,ttl)   printf("%d bytes from %s (%s): icmp_seq=%d ttl=%d ", (b), (a), (a), (seq), (ttl));

extern void icmp_start(int argc, char **argv);

#endif
