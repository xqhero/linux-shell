#ifndef ICMP_VARS_H
# error "Never use vars.h directly"
#endif

#include <stdint.h>
#include <sys/time.h>

/* todo */
extern const char *short_opts;
extern uint8_t icmp_ttl;
extern uint16_t icmp_interval;
extern uint16_t icmp_package_size;
extern uint16_t icmp_count;
extern struct timeval icmp_wait_timeout;
extern int icmp_sndbuf_size;
