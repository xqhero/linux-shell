#ifndef __TOOL_H
#define __TOOL_H

#include <stdint.h>
#include <sys/time.h>
#include <string.h>

extern void createData(char * data, uint16_t length);
extern void genRandomString(char *data, int length);
extern double mydifftime(struct timeval *start);
extern uint16_t my_chksum(uint16_t *data, int len);

#endif