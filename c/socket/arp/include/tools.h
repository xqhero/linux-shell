#ifndef ARP_TOOLS_H
#define ARP_TOOLS_H

#include <net/if.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

#define IP_ALEN 16
/*
* 定义接口的基本信息
*/
struct ifi_rinfo {
    char ifname[IF_NAMESIZE];  // 接口名字
    struct sockaddr_in *ipaddr;  // 接口的ip地址网络字节序
    uint8_t hwaddr[ETHER_ADDR_LEN];  // 接口硬件地址
    char ipaddr_str[IF_NAMESIZE];  // 接口的点分十进制ip地址
    int ifindex;  // 接口索引
    int flags;  // 接口标志
};

struct ifi_rinfo * get_ifi_rinfo(char *); 

#endif