#ifndef MY_ARP_H
#define MY_ARP_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <net/ethernet.h>
#include <stdint.h>
#include <net/if.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <stdlib.h>
#include <netpacket/packet.h>
#include <net/if_arp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <tools.h>

#undef ARP_ERROR
# define ARP_ERROR -1
#undef ARP_OK
# define ARP_OK 0

#define ARP_VERSION "1.0"
#define RCVBUFSIZE 2048

struct arp_packet {
    // DLC header
    uint8_t mac_target[ETHER_ADDR_LEN];
    uint8_t mac_source[ETHER_ADDR_LEN];
    uint16_t ether_type;

    // arp header
    uint16_t hw_type;
    uint16_t proto_type;
    uint8_t mac_addr_len;
    uint8_t ip_addr_len;
    uint16_t opcode;
    uint8_t mac_sender[ETHER_ADDR_LEN];
    uint8_t ip_sender[4];
    uint8_t mac_receiver[ETHER_ADDR_LEN];
    uint8_t ip_receiver[4];
    uint8_t padding[18]; // 做padding
};

int sockraw(int seconds);
int check_flags(struct ifi_rinfo *);
int arp_send(int sockfd, struct ifi_rinfo *rinfo, char *dstaddr, struct sockaddr_ll *addr) ;
void arp_recv(int sockfd, struct ifi_rinfo *rinfo, char *dstaddr);
#endif