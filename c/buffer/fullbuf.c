#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

    printf("%d\n", BUFSIZ);

    FILE *fp = fopen("./test.txt", "w+");
    if ( NULL == fp) {
        perror("open file failed");
        return -1;
    }
    setbuf(fp,NULL);
    char buf[] = "this is a test\n";
    fwrite(buf, sizeof(char),sizeof(buf),fp);
    // fflush(fp);
    
    sleep(20);
    fclose(fp);
    return 0;
}