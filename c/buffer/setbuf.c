#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    char ch;
    setbuf(stdin,NULL);
    setbuf(stdout,NULL);
    while(1){
        ch = getchar();
        printf("%c",ch);
    }
    return 0;
}