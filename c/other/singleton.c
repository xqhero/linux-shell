#include <stdio.h>
#include <stdlib.h>

struct singleton_t {
    int val;
};

struct singleton_t *Instance = NULL;

struct singleton_t * getInstance() {

    if(Instance == NULL) {
        Instance = (struct singleton_t *)malloc(sizeof(struct singleton_t));
        Instance -> val = 8848;
    }
    return Instance;
}

int main() {
    struct singleton_t *inst = getInstance();
    printf("hello: %p\n",inst);
    return 0;
}