#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct singleton_t {
    int val;
};
pthread_mutex_t lock;  // 线程锁

struct singleton_t *Instance = NULL;

struct singleton_t * getInstance() {
    pthread_mutex_lock(&lock);
    if(Instance == NULL) {
        Instance = (struct singleton_t *)malloc(sizeof(struct singleton_t));
        Instance -> val = 8848;
    }
    pthread_mutex_unlock(&lock);
    return Instance;
}

int main() {
    struct singleton_t *inst = NULL;
    if(pthread_mutex_init(&lock, NULL) != 0) {
        perror("pthread_mutex_init");
        return -1;
    } 
    inst = getInstance();
    printf("hello: %p\n",inst);
    return 0;
}