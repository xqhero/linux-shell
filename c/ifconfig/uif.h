#ifndef UIF_H
#define UIF_H

#include <net/if.h>
#include <stdint.h>
#include <errno.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define IFI_NAME_LEN  16  /* 定义接口的名字长度 */
#define IFI_HADDR_LEN 8   /* 定义MAC地址长度*/

struct ifi_info {
    uint8_t ifi_name[IFI_NAME_LEN];
    uint16_t ifi_index;
    uint16_t ifi_mtu;
    uint8_t  ifi_haddr[IFI_HADDR_LEN];
    uint16_t ifi_hlen;
    uint16_t ifi_flags;
    uint16_t ifi_myflags;
    uint16_t ifi_metric;
    uint16_t ifi_qlen; 
    struct sockaddr *ifi_addr;
    struct sockaddr *ifi_brdaddr;
    struct sockaddr *ifi_dstaddr;
    struct sockaddr *ifi_netmask;
    struct ifi_info *ifi_next;
};

struct ifi_info * get_ifi_info();
void free_ifi_info(struct ifi_info *);

#endif