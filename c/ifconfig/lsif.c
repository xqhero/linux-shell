#include <stdio.h>
#include <net/if.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>


int main() {
    int sockfd, len, i;
    char buf[2048], addrstr[INET_ADDRSTRLEN];

    struct ifconf ifc;
    struct ifreq *ifr;
    struct sockaddr_in *sinptr;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    ifc.ifc_len = sizeof(buf);
    ifc.ifc_req = (struct ifreq *) buf;

    ioctl(sockfd, SIOCGIFCONF, &ifc);
    
    len = ifc.ifc_len / sizeof(struct ifreq); 

    ifr = (struct ifreq *) buf;
    for (i=0; i< len; i++) {
        switch (ifr->ifr_addr.sa_family) {
            case AF_INET: 
                sinptr = (struct sockaddr_in *) &ifr->ifr_addr;
                printf("%s\t%s\n", ifr->ifr_name,
                        inet_ntop(AF_INET, &sinptr->sin_addr, addrstr, sizeof(addrstr)));
                break;
            default:
                printf("%s\n", ifr->ifr_name);
			    break;
        }
        ifr++;
    } 

    return 0;
}