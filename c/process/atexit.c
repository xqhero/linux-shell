#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void before_exit(void) {
    printf("before_exit\n");
}
void clean(void) {
    printf("before_clean\n");
}
int main(int argc, char *argv[]) {

    atexit(before_exit);
    atexit(clean);
    exit(0);
}