#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    pid_t pid;

    pid = fork();

    if(pid < 0) {
        perror("fork");
        return -1;
    } else if(pid == 0) {
        int i;
        for(i=0; i< 5; i++){
            printf("this is the child\n");
            sleep(1);
        }
        exit(3);
    } else {
        int status;
        waitpid(0, &status, 0);
        if(WIFEXITED(status)) {
            printf("child exited with code %d\n", WEXITSTATUS(status));
        } else if(WIFSIGNALED(status)) {
            printf("child terminated abnormally, signal %d\n", WTERMSIG(status));
        }
    }
    return 0;
}