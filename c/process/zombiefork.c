#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    pid_t pid;
    if((pid = fork()) < 0){
        perror("fork");
        return -1;
    } else if (pid == 0) {
        if ((pid = fork()) < 0 ) {
            perror("fork");
            return -1;
        } else if(pid > 0) {
            exit(0);
        }
        sleep(10);
        printf("second child, parent pid=%d\n", getppid());
        exit(0);
    }

    if(waitpid(pid, NULL, 0) != pid) {
        printf("waitpid error\n");
        return -1;
    }
    return 0;
}