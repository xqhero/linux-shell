#include <stdio.h>
#include <unistd.h>

// int main(int argc, char *argv[], char *envp[]) {
//     char **p = envp;
//     while(*p) {
//         printf("%s\n", *p);
//         p++;
//     }
//     return 0;
// }

int main(int argc, char *argv[]) {

    extern char **environ;
    int i =0;
    for(; environ[i] != NULL; i++) {
        printf("%s\n", environ[i]);
    }
    return 0;
}