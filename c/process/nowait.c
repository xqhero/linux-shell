#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int  main(int argc, char *argv[]) {
    pid_t pid;
    printf("process\n");
    pid = wait(NULL);
    if(pid < 0) {
        perror("wait");
        return -1;
    }
    return 0;
}