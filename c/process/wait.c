#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

void pr_exit(int);

int main(int argc, char *const argv[]) {

    pid_t pid;

    int status;

    if (fork() == 0){
        printf("this is the child process, pid=%d\n", getpid());
        sleep(30);
        _exit(10);
    } else {
        //sleep(1);
        printf("this is the parent process, wait for child...\n");
        pid = wait(&status);
        printf("child's pid=%d\n", pid);
        pr_exit(status);
    }
    return 0;
}

void pr_exit(int status) {
    if (WIFEXITED(status)) {
        printf("normal termination, exit status = %d\n", WEXITSTATUS(status));
    } else if(WIFSIGNALED(status)) {
        printf("abnormal termination, signal number = %d%s\n", 
        WTERMSIG(status),
#ifdef WCOREDUMP
        WCOREDUMP(status) ? "core file generated" : "");
#else 
        "");
#endif
    } else if(WIFSTOPPED(status)) {
        printf("child stopped, signal number i= %d\n", WSTOPSIG(status));
    }
}