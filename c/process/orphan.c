#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    pid_t pid;
    pid = fork();
    if(pid == 0) {
        sleep(30);
    } else if(pid > 0) {
        sleep(10);
    } else {
        perror("fork");
        return -1;
    }
    return 0;
}