#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    char *p;
    p = getenv("USER");
    if(p){
       printf("USER=%s\n",p); 
    }
    setenv("USER","ABC",1);
    printf("USER=%s\n", getenv("USER"));
    unsetenv("USER");
    printf("USER=%s\n", getenv("USER"));
}