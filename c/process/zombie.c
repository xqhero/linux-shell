#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    pid_t pid = fork();
    if(pid == 0) {
        
    } else if(pid > 0) {
        printf("parent process\n");
        sleep(30);
    } else {
        perror("fork");
        return -1;
    }
    return 0;
}