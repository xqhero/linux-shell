#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    pid_t pid = vfork();

    if (pid < 0) {
        perror("vfork");
        return -1;
    } else if(pid == 0) {
        printf("child process, pid=%d, ppid=%d\n", getpid(), getppid());
        sleep(2);
    } else {
        printf("parent process, pid=%d, ppid=%d\n", getpid(), getppid());
    }
    return 0;
}