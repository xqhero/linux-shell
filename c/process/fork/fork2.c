#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int globval = 6;
char buf[] = "a write to stdout\n";


int main(int argc, char * argv[]) {

    int var;
    pid_t pid;

    var = 88;
    if (write(STDOUT_FILENO, buf, sizeof(buf)-1) != sizeof(buf)-1) {
        printf("write error\n");
        exit(-1);
    }

    printf("before_fork\n");
    if((pid = fork()) < 0) {
        perror("fork");
        exit(-1);
    } else if(pid ==0) {
        globval++;
        var++;
    } else {
        sleep(2);
    }

    printf("pid=%d, glob=%d, var=%d\n", getpid(), globval,var);
    return 0;
}