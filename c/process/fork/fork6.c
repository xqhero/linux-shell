#include <stdio.h>
#include <unistd.h>
#include <errno.h>

/*
* 查看复制的内容
*/
int main(int argc, char *argv[]) {
    pid_t pid;
    pid = fork();
    if(pid < 0) {
        perror("fork");
        return -1;
    }
    sleep(20);
    return 0;
}