#include <stdio.h>
#include <errno.h>
#include <unistd.h>

/*
* 子进程的个数
*/
int main(int argc, char *argv[]) {

    int i = 0;
    pid_t pid;
    for (i=0; i < 3; i++) {
        pid = fork();
    }
    if(pid ==0) {
        printf("child process, pid:%d, ppid:%d\n",getpid(), getppid());
    } else {
        printf("parent process, pid:%d, ppid:%d\n", getpid(), getppid());
    }
    sleep(1);
    return 0;
}