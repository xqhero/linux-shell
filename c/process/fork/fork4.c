#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {

    int i=0;
    pid_t pid;
    for(i=0; i<3; i++) {
        pid = fork();
        if(pid < 0) {
            perror("fork");
            break;
        }
        if(pid == 0) break;
    }
    printf("pid=%d,ppid=%d\n",getpid(),getppid());
    if(pid > 0){
        for(i=0;i<3;i++){
            wait(NULL);
        }
    }
    return 0;
}