#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define ECHO(s) printf("%s pid=%d, ppid=%d, uid=%d, euid=%d,gid=%d,egid=%d\n", \
            s, \
            getpid(), \
            getppid(), \
            getuid(), \
            geteuid(), \
            getgid(), \
            getegid() \
        )

int main(int argc, char *argv[]) {

    pid_t pid;

    if ((pid = fork()) < 0) {
        perror("fork");
        return -1;
    }

    if(pid == 0) {
        ECHO("子进程");
        exit(0);
    }

    if (pid > 0){
        ECHO("父进程");
    }

    sleep(5);
    return 0;

}