#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
    int count = 0;
    pid_t pid=vfork();
    int i =0 ;
    if(pid < 0) {
        perror("fork");
        return -1;
    }else if(pid == 0) {
        printf("child process, pid=%d, ppid=%d\n", getpid(),getppid());
        for(; i< 5; ++i) {
            printf("count = %d ", ++count);
            printf("I am a child pid: %d, return: %d\n", getpid(),pid);
        }
        exit(0);
    }else {
        printf("parent process, pid=%d, ppid=%d\n", getpid(),getppid());
        printf("i= %d\n", i);
        for(; i< 5; ++i) {
            printf("count = %d ", ++count);
            printf("I am a parent pid: %d, return: %d\n", getpid(),pid);
        }
    }
    return 0;
}