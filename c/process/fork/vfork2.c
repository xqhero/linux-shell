#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
    // int i = 100;
    pid_t pid = vfork();
    int i = 100;
    if(pid < 0) {
        perror("vfork");
        return -1;
    }else if(pid == 0) {
        i++;
        printf("子进程中的i=%d\n",i);
        sleep(5);
        exit(0);
    }else {
        printf("父进程中i=%d\n",i);
    }
    return 0;
}