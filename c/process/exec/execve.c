#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    printf("entering main process\n");
    char *envp[] = {"A=1",NULL};
    char * const argvs[] = {"env",NULL};
    execve("/bin/env",argvs,envp);
    return 0;
}