#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

    printf("entering main process\n");
    char *const argvs[] = {"ls", "-al", "/etc/passwd", NULL};
    execvp("ls",argvs);
    return 0;
}