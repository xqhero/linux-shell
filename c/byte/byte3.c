#include <stdio.h>
// 相临不同类型
typedef struct st
{
    unsigned char a:2;
    unsigned char :0;
    unsigned char b:5;
    unsigned int c:5;
};

int main() {
    struct st bit;
    bit.a = 1;
    bit.b = 3;
    bit.c = 7;
    printf("%d,%d,%d\n", bit.a, bit.b, bit.c);
    // 输出整个的结构体
    int len = sizeof(bit);
    char *p = (char *)&bit;
    int i = 0;
    printf("size = %d\n", len);
    for(; i < len; i++) {
        printf("%02x\t", *(p+i));
    } 
    printf("\n");
    return 0;
}