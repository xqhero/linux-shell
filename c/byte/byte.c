#include <stdio.h>
// 比较位域与普通类型的内存大小
typedef struct {
    unsigned int x;
    unsigned int y;
} status1 ;

typedef struct {
    unsigned int x : 1;
    unsigned int y : 2;
} status2 ;

int main(){
    status1 s1;
    status2 s2 = {1, 3};
    printf("size of s1=%d\n", sizeof(s1));
    printf("size of s2 = %d\n", sizeof(s2));
    return 0;
}







