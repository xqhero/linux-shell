#include <stdio.h>

typedef union{
          unsigned char hex;
         struct{
              unsigned char low  : 4;
              unsigned char high : 4;
          };
}convert;

int main() {

    convert t ;
    t.low = 0x01;
    t.high = 0x02;

    printf("hex = 0x%0x\n", t.hex);
    return 0;
}