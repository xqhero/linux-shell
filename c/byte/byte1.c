#include <stdio.h>
// 相同类型且宽度小于类型的宽度
typedef struct st
{
    unsigned char a:1;
    unsigned char b:2;
    unsigned char c:3;
};

int main() {
    struct st bit;
    bit.a = 1;
    bit.b = 3;
    bit.c = 7;
    printf("%d,%d,%d\n", bit.a, bit.b, bit.c);
    // 输出整个的结构体
    int len = sizeof(struct st);
    char *p = (char *)&bit;
    int i = 0;
    printf("size = %d\n", len);
    for(; i < len; i++) {
        printf("%2x\t", *(p+i));
    } 
    printf("\n");
    return 0;
}