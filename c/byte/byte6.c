#include <stdio.h>
#include <stdint.h>

int main(){

    int16_t a = -3;
    uint8_t *p = (uint8_t *)&a;
    int len = sizeof(a);
    int i;
    printf("%d\n", len);
    for(i=0; i< len; i++) {
        printf("%02x\t",*(p+i));
    }
}