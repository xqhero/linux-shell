#include <stdio.h>
// 穿插非位域类型
typedef struct st
{
    unsigned char a:2;
    unsigned char :0;
    unsigned int c:5;
    long d;
};

int main() {
    struct st bit;
    // 输出整个的结构体
    int len = sizeof(bit);
    printf("size = %d\n", len);
    return 0;
}