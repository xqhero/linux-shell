#include <stdio.h>
#include <getopt.h>

static char *short_opts = "h:u:p:";
static struct option long_options[] = {
    {"host", optional_argument, NULL, 'h'},
    {"user", required_argument, NULL, 'u'},
    {"password", required_argument, NULL, 'p'},
    {"help", no_argument, NULL, '?'},
    {NULL, 0, NULL, 0}
};

static void printHelp(char *);

int main(int argc, char *argv[]) {
    char *host = "127.0.0.1";
    char *user = "root";
    char *password = "";
    
    int opt = 0;
    while((opt = getopt_long(argc, argv, short_opts, long_options, NULL)) != -1){
        switch (opt) {
            case '?':
                printHelp(argv[0]);
                return 0;
            case 'h':
                host = optarg;
                break;
            case 'u':
                user = optarg;
                break;
            case 'p':
                password = optarg;
                break;
        }
    }
    printf("host=%s, username=%s, password=%s\n",host,user,password);
    return 0;
}

void printHelp(char *prog) {
    printf("%s usage.\n", prog);
    printf("eg:\n");
    printf("%s -h localhost -u root -p 123456789\n", prog);
    printf("-h: hostname or ipaddress, default: localhost\n");
    printf("-u: user, default: root\n");
    printf("-p: port number, default: 3306\n");
}