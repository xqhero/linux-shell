#include <stdio.h>
#include <sys/sysctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <stdlib.h>

char * net_rt_iflist(int family, int flags, size_t *lenp) {
    int mib[6];
    char *buf;

    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = family;
    mib[4] = NET_RT_IFLIST;
    mib[5] = flags;

    if (sysctl(mib, 6, NULL, lenp, NULL, 0) < 0) {
        return NULL;
    }
    if ((buf = calloc(*lenp,1)) == NULL) {
        return NULL;
    }
    if (sysctl(mib, 6 , buf, lenp, NULL, 0) < 0) {
        free(buf);
        return NULL;
    }
    return buf;
}

int main() {
    size_t lenp;
    char *ptr;
    ptr = net_rt_iflist(AF_INET, 0 , &lenp);
    printf("%d\n",lenp);
    return 0;

}