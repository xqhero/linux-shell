#include <stdio.h>

#define swap(type,a,b) do{ \
        type t = a; \
        a = b; \
        b =t; \
    }while(0)

void sort(int *array, int size, int (*sortfun)(int a, int b)) {
    int i = 1,j;
    while (i < size) {
        j = i;
        while(j > 0) {
            if(sortfun(array[j], array[j-1]) < 0) {
                swap(int, array[j], array[j-1]);
                j--;
            } else {
                break;
            }
        }
        i++;
    }
}

int less(int a , int b) {
    if ( a >b ) {
        return 1;
    } else if (a < b) {
        return -1;
    }else {
        return 0;
    }
}

int main() {
    int arr[5] = {1,5,3,6,2};
    int i = 0;
    sort(arr, 5, less);
    for(; i < 5; i++){
        printf("%d\t", arr[i]);
    }
    return 0;
}