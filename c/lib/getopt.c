#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int ch;
    opterr = 1;
    while( (ch = getopt(argc, argv, "a:b:c:d:e:")) != -1) {
        switch (ch) {
            case 'a':
            printf("option a :%s\n", optarg);
            break;
            case 'b':
            printf("option b : %s\n", optarg);
            break;
            case '?':
	    	printf("cant't support\n");
		return -1;
        }
    }
    printf("optind %d\n",optind);
    printf("optopt %c\n",optopt);
}
