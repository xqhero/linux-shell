#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int g_A = 10;
int g_B  = 20;
static int g_C = 30;
static int g_D;
int g_E;
char *p1;

int main(int argc, char *argv[]){
    int local_A;
    static int local_C = 0;
    static int local_D;

    char *p3 = "123456";

    p1 = (char *) malloc(10);

    strcpy(p1, "123456");

    printf("\n");
    printf("main address: %p\n", main);
    printf("g_A addrss, %p\n", &g_A);
    printf("g_B addrss, %p\n", &g_B);
    printf("g_C addrss, %p\n", &g_C);
    printf("g_D addrss, %p\n", &g_D);
    printf("g_E addrss, %p\n", &g_E);
    printf("p1 addrss, %p\n", &p1);

    printf("local_A addrss, %p\n", &local_A);
    printf("local_C addrss, %p\n", &local_C);
    printf("local_D addrss, %p\n", &local_D);

    printf("p3 addrss, %p\n", &p3);
    return 0;
}