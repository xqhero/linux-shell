#include <stdio.h>
#include <time.h>

int main(){
    time_t ta;
    struct tm * t;
    time(&ta);
    t = localtime(&ta);
    printf("tm_sec = %d\n", t->tm_sec);
    printf("tm_min = %d\n", t->tm_min);
    printf("tm_hour = %d\n", t->tm_hour);
    printf("tm_mday = %d\n", t->tm_mday);
    printf("tm_mon = %d\n", t->tm_mon + 1);
    printf("tm_year = %d\n", t->tm_year + 1900);
    printf("tm_wday = %d\n", t->tm_wday);
    printf("tm_isdst = %d\n", t->tm_isdst);
    printf("tm_gmtoff = %ld\n", t->tm_gmtoff);
    printf("tm_zone = %s\n", t->tm_zone);
}