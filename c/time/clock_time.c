#include <stdio.h>
#include <time.h>

int main() {
    struct timespec t;
    printf("result = %d\n",clock_gettime(CLOCK_REALTIME, &t));
    printf("%ld, %ld\n",t.tv_sec, t.tv_nsec);
}