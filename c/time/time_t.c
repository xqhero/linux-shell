#include <stdio.h>
#include <time.h>

int main(){
    time_t t;
    if(time(&t) == -1) {
        perror("time");
        return 1;
    }
    printf("%ld\n",t);
}