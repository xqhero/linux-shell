#include <stdio.h>
#include <sys/time.h>

int main() {
    struct timeval tv;
    if (-1 == gettimeofday(&tv, NULL)){
        perror("gettimeofday()");
        return 1;
    }
    printf("size=%lu\n",sizeof(tv));
    printf("tv_sec = %ld\n", tv.tv_sec);
    printf("tv_usec = %d\n", tv.tv_usec);
}